
```{post} 2023-07-18
:author: "@Schadenfreud"
:tags: participante
:category: miembros
:language: Español
:location: Colombia
:excerpt: 1
```

# @guayahack

Hola soy `@Schadenfreud`! 

Soy Stiven. Estoy muy interesado en aprender programación porque es un mundo que siempre me ha gustado pero que jamás he practicado. Espero que esta sea la mejor oportunidad.

## TODO

```{figure} index.md-data/tux.png
---
name: guayahack-tux
---
Tux
```

```console
$ git status 
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
