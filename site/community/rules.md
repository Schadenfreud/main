
# Reglas

%#todo:escribir

## 0x01

El respeto y la cordialidad entre los miembros en sus interacciones priman ante *todo*.

## 0x02

La voluntad de aprender y el conocimiento son el propósito que reune a los miembros. Cada participante cuenta con información valiosa desde su experiencia y ha de compartirla, en la medida de lo posible, sin reservas a fin de contribuir al crecimiento de los demás.

## 0x03

Las actividades, retos y dinámicas generadas dentro de la comunidad se dan *sin ánimo de lucro* y se rigen bajo la licencia de uso abierto *con atribución*.

## 0x04

La participación de todos los miembros es voluntaria y, si bien colectivamente guiada, en la medida de lo posible autónoma y se dará de acuerdo a sus capacidades y disponibilidad. 

## 0x05

A fin de propiciar el aprendizaje efectivo y eficiente al igual que el entendimiento mutuo, todo participante hará uso del lenguaje más preciso y detallado que le sea posible.

## 0x06

A fin de perpetuar la viabilidad del espacio en el futuro, los miembros procurarán siempre documentar, consignar, codificar y compartir sus herramientas, técnicas y procedimientos en cualquiera sea el espacio apropiado para ello. 

## 0x07

Discusiones centradas en la política, la religión y el sexo, si bien son temas con relevancia social, dividen y a menudo generan conflictos. GuayaHack *no* es el lugar para éste tipo de discusiones.

## 0x08

Los {doc}`/wiki/organizacion-rol-moderator` podrán marcar, remover y filtrar contenido que se aleje de la temática principal de la comunidad tanto en todas sus formas y en todos los espacios.

## 0x09

Éstas reglas existen a fin de garantizar que GuayaHack cumpla con su {doc}`/community/memorial` y serán adaptadas conforme sea necesario.

## 0x0A

Nuestro medio de comunicación oficial es el servidor de Discord. A través de la plataforma de gestionarán los espacios, discusiones, encuentros y demás. Asimismo, todo suceso relevante se publicará en {doc}`/noticias` y [#rules](https://discord.gg/kPUKbKNP). Contactar a miembros por fuera de Discord sin que éstos hayan expresado interés no es aceptable.

## 0x0B 

Toda actividad conducida por, en y con GuayaHack tiene como propósito educar a sus miembros y estos se encuentran en la obligación de utilizar su conocimiento con fines exclusivamente *éticos y legales*
